<?php

const JSON_PATH = 'https://design.propcom.co.uk/buildtest/accordion-data.json';

class BaseModel
{

    public static function fetchJsonData()
    {

        try {
            $result = file_get_contents(JSON_PATH);
        }
        catch(Exception $e) {
            echo "Json couldn't be loaded: " . $e->getMessage();
        }

        return json_decode($result);
    }

}
<?php
    include 'View/partials/header.php';
    include 'Model/BaseModel.php';

    $data = BaseModel::fetchJsonData();
?>
    <main>
        <div class="wrapper">
            <?php if(isset($data)): ?>
                <?php foreach($data->blocks as $id => $elem): ?>
                    <section>
                        <header>
                            <button class="accordion-btn"><?= $elem->heading ?></button>
                        </header>
                        <div class="text-wrap">
                            <p><?= $elem->content ?></p>
                        </div>
                    </section>
                <?php endforeach; ?>
            <?php else: ?>
                <h3 class="error">The data couldn't be loaded, please come back later.</h3>
            <?php endif; ?>
        </div>
    </main>

<?php include 'View/partials/footer.php'; ?>


document.addEventListener('DOMContentLoaded', function() {

    let buttons = document.getElementsByClassName('accordion-btn');

    for(let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', toggleActive);
    }

});

function toggleActive() {
    this.closest('section').classList.toggle('active');
    let text = this.closest('section').querySelector('.text-wrap');
    scrollToggle(text);
}

function scrollToggle(elem) {

    if(elem.closest('section').classList.contains('active')) {
        elem.style.height = elem.scrollHeight + 'px';
    }
    else {
        elem.style.height = '0px';
    }

}
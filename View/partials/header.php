<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="author" content="Peter Toth">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Resources -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:700i&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="View/css/style.css?v=3">

    <!-- JS -->
    <script src="View/script/main.js"></script>

    <title>Accordion for Propeller Digital</title>
</head>
<body>